<?php

namespace App\Http\Controllers\User;

use App\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActividadesController extends Controller {

    /**
     *  Muestra un listado de todas las actividiades que hay en la Base de Datos.
     *
     *  @return Redirecciona a la tabla de reservas del usuario logeado, con un mensaje.
     */

    public function index() {
        $eventos = Event::where([['start_date', '>', Carbon::now()]])->orderBy('start_date', 'asc')->paginate(4);

        return view('user.actividades.index')->with('actividades', $eventos);
    }

    /**
     *  Muestra una actividad en concreto.
     *
     *  @param  $id Identidicador de la actividad.
     *  @return Redirecciona al formulario donde muestra una actividad en concreto.
     */

    public function show($id) {
        $eventos = Event::find($id);

        return view('user.actividades.show')->with('actividad', $eventos);
    }
}
