<?php

namespace App\Http\Controllers\User;

use App\Event;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsuariosController extends Controller {

    public function users() {
        $users = User::whereHas('roles', function($q) { $q->whereIn('name', ['admin', 'mod']); })->paginate(10);

        $id =Auth::user()->id;
        $user = User::find($id);

        return view('user.follow.users', compact('users','user'));
    }

    public function user($id) {
        $user = User::find($id);
        $actividades = Event::where([['user_id', $user->id], ['start_date', '>', Carbon::now()]])->orderBy('start_date', 'asc')->paginate(10);

        return view('user.follow.actividadesList')->with('user', $user)->with('actividades', $actividades);
    }

    public function ajaxRequest(Request $request) {
        $user = User::find($request->user_id);

        $response = auth()->user()->toggleFollow($user);

        return response()->json(['success'=>$response]);
    }
}
