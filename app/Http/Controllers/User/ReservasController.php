<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Event;
use App\User;
use Auth;

class ReservasController extends Controller {
    /**
     *  Muestra un listado con las reservas del usuario logeado.
     *
     *  @return Devuelve la vista de reservas.index con el usuario logeado.
     */

    public function index() {
        $id = Auth::id();
        $usuario = User::find($id);

        return view('user.reservas.index')->with('usuario', $usuario);
    }

    /**
     *  Elimina una reserva que tiene el usuario logeado.
     *
     *  @param  $id Identidicador de la actividad.
     *  @return Redirecciona a la tabla de reservas del usuario logeado, con un mensaje.
     */

    public function destroy($id) {
        $actividad = Event::find($id);
        $idUsuario = Auth::id();

        //Elimina la relacion que hay entre el usuario logeado y la actividad seleccionada.
        $actividad->users()->detach($idUsuario);



        return redirect()->route('user.reservas.index')->with('success','Se eliminado su reserva.');
    }

    /**
     *  Crea una reserva al usuario que hay logeado.
     *
     *  @param $id Identidicador de la actividad.
     *  @return Redirecciona a la tabla de reservas del usuario logeado, con un mensaje.
     */

    public function crear($id){
        $idUsuario = Auth::id();
        $user = User::find($idUsuario);

        $actividad = Event::find($id);

        if($user->events->contains($id)){
            return redirect()->route('user.actividades.index')->with('success','Ya tienes una reserva para esta actividad.');

        }else if($this->comprobar($actividad)){
            return redirect()->route('user.actividades.index')->with('success','Ya tienes una reserva para esta fecha.');

        }

        $actividad->users()->attach($idUsuario);
        return redirect()->route('user.reservas.index')->with('success','Se creado su reserva.');
    }

    /**
     *  Comprobar que no tengamos una reserva para una actividad en la misma fecha y hora que otra actividad ya reservada.
     *
     *  @param $activiadRecibida Actividad a la que queremos hacer la reserva.
     *  @return true = si tenemos una reserva para las mismas fechas.
     */

    public function comprobar($activiadRecibida){
        $idUsuario = Auth::id();
        $user = User::find($idUsuario);

        $inicioActividad = $activiadRecibida->start_date;
        $finActividad = $activiadRecibida->end_date;

        foreach($user->events as $evento){
            if($evento->start_date == $inicioActividad && $evento->end_date == $finActividad){
                return true;
            }
        }

        return false;
    }
}
