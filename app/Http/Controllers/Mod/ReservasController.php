<?php

namespace App\Http\Controllers\Mod;

use App\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ReservasController extends Controller
{
    /**
     *  Devuelve una lista de las actividades.
     *
     * @return Devuelve la vista 'admin.reservas.index'
     */

    public function index() {
        $user = Auth::user();
        $idUsuario = $user->id;

        $eventos = Event::where([['user_id', $idUsuario], ['start_date', '>', Carbon::now()]])->orderBy('start_date', 'asc')->paginate(10);

        return view('mod.reservas.index')->with('reservas',$eventos);
    }

    /**
     *  Muestra un listado de las reservas de una actividad en concreto.
     *
     * @param  int  $id Identificador de el evento seleccionado.
     * @return Devuelve la vista 'admin.reservas.show'
     */

    public function show($id) {
        $evento = Event::find($id);

        return view('mod.reservas.show')->with('evento', $evento);
    }

}
