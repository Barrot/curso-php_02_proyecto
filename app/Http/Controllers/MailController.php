<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class MailController extends Controller
{

    /**
     *  Envia un correo elecontrico a la cuenta de WeTask
     *
     *  @param  Request $request Datos del usuario que envia el correo
     */

    public function sendMail(Request $request) {

        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'mensaje' => 'required',
        ]);

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['mensaje'] = $request->mensaje;

        Mail::send('mail.contacto', $data, function($message) {
            $message->to('wetask.calendarios@gmail.com', 'WeTask    ')->subject('Usuario');
        });
    }
}
