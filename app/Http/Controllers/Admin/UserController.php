<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Role;

class UserController extends Controller {
    /**
     *  Muestra un listado de usuarios.
     *
     * @return Devuelve la vista 'admin.users.index' con los usuarios (users)
     */
    public function index() {
        return view('admin.users.index')->with('users', User::paginate(10));
    }

    /**
     *  Devuelve el formulario para editar los roles de un usuario.
     *
     *  @param  int  $id Identificador del usuario.
     *  @return Devuelve la vista 'admin.users.index' con un mensaje.
     */
    public function edit($id) {
        if(Auth::user()->id == $id){
            return redirect()->route('admin.users.index')->with('warning','No puedes editar tu propio usuario.');
        }

        return view('admin.users.edit')->with(['user' => User::find($id), 'roles' => Role::all()]);
    }

    /**
     *  Actualiza los roles de un usaurio.
     *
     * @param   $request Contenido actualizado de un usaurio.
     * @param  int  $id Identificador del usuario.
     * @return Devuelve la vista 'admin.users.index' con un mensaje.
     */
    public function update(Request $request, $id) {
        if(Auth::user()->id == $id){
            return redirect()->route('admin.users.index')->with('warning','No puedes editar tu propio usuario.');
        }

        $user = User::find($id);
        $user->roles()->sync($request->roles);

        return redirect()->route('admin.users.index')->with('success','Se ha editado correctamente.');
    }

    /**
     *  Elimina un usuario.
     *
     *  @param  int  $id Identificador de un usuario
     *  @return Devuelve la vista 'admin.users.index' con un mensaje.
     */
    public function destroy($id) {
        if(Auth::user()->id == $id){
            return redirect()->route('admin.users.index')->with('warning','No puedes eliminar tu propio usuario.');
        }

        $user = User::find($id);

        if($user){
            $user->roles()->detach();
            $user->delete();
            return redirect()->route('admin.users.index')->with('success','Se ha eliminado correctamente.');
        }

        return redirect()->route('admin.users.index')->with('warning','Este usuario no puede ser eliminado.');
    }
}
