<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReservasController extends Controller
{
    /**
     *  Devuelve una lista de las actividades.
     *
     * @return Devuelve la vista 'admin.reservas.index'
     */

    public function index() {
        $eventos = Event::where([['start_date', '>', Carbon::now()]])->orderBy('start_date', 'asc')->paginate(10);

        return view('admin.reservas.index')->with('reservas',$eventos);
    }

    /**
     *  Muestra un listado de las reservas de una actividad en concreto.
     *
     * @param  int  $id Identificador de el evento seleccionado.
     * @return Devuelve la vista 'admin.reservas.show'
     */

    public function show($id) {
        $evento = Event::find($id);

        return view('admin.reservas.show')->with('evento', $evento);
    }

}
