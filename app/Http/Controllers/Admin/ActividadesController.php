<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use App\Event;
use Auth;
use File;
use PDF;

class ActividadesController extends Controller
{
    /**
     *  Mostrar un listado de las actividades que hay en la Base de datos que pertenezen al usuario logeado.
     *
     *  @return Devuelve la vista 'admin.actividades.index'.
     */

    public function index() {
        $eventos = Event::where([['start_date', '>', Carbon::now()]])->orderBy('start_date', 'asc')->paginate(10);

        return view('admin.actividades.index')->with('actividades',$eventos);
    }

    /**
     *  Devuelve la vista de un fomrulario para crear una actividad.
     *
     *  @return Devuelve la vista 'admin.actividades.create'
     */

    public function create() {
        return view('admin.actividades.create');
    }

    /**
     *  Crea una nueva actividad.
     *
     * @param Request $request Datos de una actividad.
     * @return Redirecciona al listado de actividades con un mensaje.
     */

    public function store(Request $request) {
        $user = Auth::user();

        $request->validate([
            'title' => 'required|string' ,
            'descripcion' => 'required|string' ,
            'inicio' => 'required',
            'fin' => 'required|after_or_equal:inicio',
        ]);

        $imageName = time().'.'.request()->imagen->getClientOriginalExtension();
        request()->imagen->move(public_path('img/actividades'), $imageName);


        $event = new Event([
            'title' => $request->input('title'),
            'descripcion' => $request->input('descripcion'),
            'start_date' => $request->input('inicio'),
            'end_date' => $request->input('fin'),
            'imagen' => $imageName,
            'user_id' => $user->id
        ]);

        $event->save();

        return redirect()->route('admin.actividades.index')->with('success','Se ha CREADO correctamente.');
    }

    /**
     *  Muestra en un formulario una actividad en concreto.
     *
     *  @param  $id Identificador de la actividad.
     *  @return Devuelve la vista 'admin.actividades.show', con el evento en concreto.
     */

    public function show($id) {
        $eventos = Event::find($id);

        return view('admin.actividades.show')->with('actividad', $eventos);
    }

    /**
     *  Devuelve un formulario para editar una actividad.
     *
     * @param  int  $id Identificador de la actividad.
     * @return Devuelve la vista 'admin.actividades.edit'.
     */

    public function edit($id) {
        return view('admin.actividades.edit')->with(['actividad' => Event::find($id)]);
    }

    /**
     *  Actualiza una actividad.
     *
     * @param  $request Datos de la actividad actualizados.
     * @param  int  $id Identificador de la actividad.
     * @return Redirecciona a la vista de 'admin.actividades.index' con un mensaje.
     */

    public function update(Request $request, $id) {
        $event = Event::find($id);

        if(request()->imagen){
            File::delete('img/actividades/'.$event->imagen);

            $imagenNueva = time().'.'.request()->imagen->getClientOriginalExtension();
            request()->imagen->move(public_path('img/actividades'), $imagenNueva);

        }else{
            $imagenNueva = $event->imagen;
        }

        $event->title = $request->input('title');
        $event->descripcion = $request->input('descripcion');
        $event->start_date = $request->input('inicio');
        $event->end_date = $request->input('fin');
        $event->imagen = $imagenNueva;

        $event->save();

        return redirect()->route('admin.actividades.index')->with('success','Se ha guardado correctamente.');
    }

    /**
     *  Elimina una actividad.
     *
     *  @param  int  $id Identificador de la actividad.
     *  @return Redirecciona a la vista de 'admin.actividades.index' con un mensaje.
     */

    public function destroy($id) {
        $evento = Event::find($id);

        if($evento){
            $evento->delete();
            File::delete('img/actividades/'.$evento->imagen);
        }

        return redirect()->route('admin.actividades.index')->with('success','Se ha eliminado correctamente.');
    }

    public function generatePDF($id) {

        $evento = Event::find($id);

        $titulo = $evento->title;

        $data = ['evento' => $evento];
        $pdf = PDF::loadView('partials/pdfView', $data);

        return $pdf->download($titulo.'.pdf');

    }
}
