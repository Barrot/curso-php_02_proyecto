<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use App\Event;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller {

    /**
     *  Guarda en una array todos los eventos.
     *
     *  @return Devuelve la vista 'home' pasando una array con todos los eventos.
     */

    public function index(){
        $events = [];
        $data = Event::all();

        if($data->count()){
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    $value->title,
                    true,
                    new \DateTime($value->start_date),
                    new \DateTime($value->end_date.' +1 day'),
                    $value->id,
                    [
                            'color' => '#FF5757',
                        'textColor' => '#FFFFFF'
                    ]
                );
            }
        }

        if(Auth::guest()){
            return view('welcome');
        } else if(Auth::user()->hasAnyRole('admin')){
            $calendar = Calendar::addEvents($events)->setOptions(['lang' => 'es'])
                ->setCallbacks(['eventClick' => 'function(event, jsEvent, view) {window.location.href = "/admin/actividades/"+event.id+"/edit"}']);
        }else if(Auth::user()->hasAnyRole('user')) {
            $calendar = Calendar::addEvents($events)->setOptions(['lang' => 'es'])
                ->setCallbacks(['eventClick' => 'function(event, jsEvent, view) {window.location.href = "/user/actividades/"+event.id;}']);
        }else if(Auth::user()->hasAnyRole('mod')){
            $calendar = Calendar::addEvents($events)->setOptions(['lang' => 'es'])
                ->setCallbacks(['eventClick' => 'function(event, jsEvent, view) {window.location.href = "/mod/actividades/ver/"+event.id;}']);
        }

        return view('home', compact('calendar'));
    }
}