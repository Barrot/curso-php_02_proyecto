<?php

namespace App\Http\Controllers;

use App\Calendario;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        return view('home');
    }

    public function update(Request $request, $id){

        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
        ]);

        $usuario = User::find($id);

        $usuario->name = $request->name;
        $usuario->email = $request->email;

        $usuario->save();

        return redirect()->route('home')->with('success','Se ha guardado correctamente.');
    }

}
