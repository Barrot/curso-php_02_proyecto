<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Event extends Model {
    protected $fillable = ['id','title', 'descripcion' ,'start_date','end_date', 'imagen', 'user_id'];

    //Reservas
    public function users(){
        return $this->belongsToMany(User::class);
    }

    //Actividades
    public function user(){
        return $this->belongsTo(User::class);
    }
}
