<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('events')->insert([
            'title' => 'Evento Ejemplo 1',
            'descripcion' => 'Descripcion Evento Ejemplo 3',
            'start_date' => '2019/09/14 00:00:00',
            'end_date' => '2019/09/14 10:00:00',
            'user_id' => 1,
        ]);

        DB::table('events')->insert([
            'title' => 'Evento Ejemplo 2',
            'descripcion' => 'Descripcion Evento Ejemplo 3',
            'start_date' => '2019/09/15 00:00:00',
            'end_date' => '2019/09/15 10:00:00',
            'user_id' => 1,
        ]);

        DB::table('events')->insert([
            'title' => 'Evento Ejemplo 3',
            'descripcion' => 'Descripcion Evento Ejemplo 3',
            'start_date' => '2019/09/16 00:00:00',
            'end_date' => '2019/09/16 10:00:00',
            'user_id' => 1,
        ]);
    }
}
