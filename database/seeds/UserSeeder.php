<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $adminRole = Role::where('name', 'admin')->first();
        $modRole = Role::where('name', 'mod')->first();
        $userRole = Role::where('name', 'user')->first();

        $admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password')
        ]);

        $mod = User::create([
            'name' => 'Moderador',
            'email' => 'mod@mod.com',
            'password' => bcrypt('password')
        ]);

        $user = User::create([
            'name' => 'User',
            'email' => 'user@user.com',
            'password' => bcrypt('password')
        ]);

        $admin->roles()->attach($adminRole);
        $mod->roles()->attach($modRole);
        $user->roles()->attach($userRole);

        factory(App\User::class, 5)->create();

    }
}