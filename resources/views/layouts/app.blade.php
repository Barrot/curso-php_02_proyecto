<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!--     Estilos CSS     -->
    @include('partials.styles')
    @yield('styles')

</head>
<body>
    <div id="app">
        <!--     TOP BAR     -->
        @include('partials.sidebar')

        <!--     CONTENIDO     -->
        <main class="py-4 container">
            @include('partials.alerts')
            @yield('content')
        </main>
    </div>

    <!--     Scripts JS / JQ     -->
    @include('partials.scripts')
    @yield('scripts')
</body>
</html>