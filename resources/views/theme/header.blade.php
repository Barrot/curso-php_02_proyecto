<!-- Header -->
<header class="masthead">
    <div class="container">
        <div class="intro-text">
            <div class="intro-heading text-uppercase">WE TASK</div>
            <div class="intro-lead-in">La red social de calendarios</div>
            <!-- Si el usuario NO esta registrado o logeado -->
            @if (Auth::guest())
                <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="{{ route('register') }}">Registrate</a>
                <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="{{ route('login') }}">Inicia sesión</a>
            @else
            <!-- Si el usuario esta registrado o logeado -->
                <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="{{ route('home') }}">Home</a>
            @endif
        </div>
    </div>




</header>
