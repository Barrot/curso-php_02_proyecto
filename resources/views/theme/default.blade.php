
<!-- Services -->
<section class="page-section" id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">¿Porque We task?</h2>
                <p>&nbsp;</p>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-calendar-alt fa-stack-1x fa-inverse"></i>
          </span>
                    <h4 class="service-heading">Todos tus calendarios</h4>
                    <p class="text-muted">En We Task encontraras todos tus calendarios agrupados en una misma página para que no te pierdas ningún evento, sea cual sea el organizador.</p>
                </div>
                <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-eye fa-stack-1x fa-inverse"></i>
          </span>
                    <h4 class="service-heading">Diseño muy visual</h4>
                    <p class="text-muted">We Task tiene un diseño muy visual e intuitivo siendo así muy fácil de usar, permitiendo que cualquier persona pueda usarlo sin ningún tipo de problema.</p>
                </div>
                <div class="col-md-4">
          <span class="fa-stack fa-4x">
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="fas fa-calendar-check fa-stack-1x fa-inverse"></i>
          </span>
                    <h4 class="service-heading">Registro en eventos</h4>
                    <p class="text-muted">En We Task no hará falta que salgas y vayas a otras páginas para registrarte a un evento. Podrás hacerlo facil y directamente desde cualquier calendario.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About -->
<section class="bg-light page-section" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Primeros pasos</h2>
                <h3 class="section-subheading">Estos son los primeros pasos basicos para poder usar nuesta web.</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <ul class="timeline">
                    <li>
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="img/about/1.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-body">
                                <p class="text-muted">Resgistrate para poder usar nuestra web, los pasos son muy sencillos. Si ya lo has hecho, tan solo inicia sesión.</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="img/about/2.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-body">
                                <p class="text-muted">Una vez dentro podrás ver en el calendario todos los eventos que hay disponibles.</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="img/about/3.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-body">
                                <p class="text-muted">Para hacer una reserva tan solo tendras que mirar el listado de actividades, hacer clic encima de una actividad y hacere clic a "Inscribirme".</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="img/about/4.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-body">
                                <p class="text-muted">Puedes seguir a usuarios con las actividades mas interesantes y ver sus actividades.</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-panel">
                        <div class="timeline-image">
                            <img class="rounded-circle img-fluid" src="img/about/5.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-body">
                                <p class="text-muted">Puedes cancelar tus reservas sin ningun problema.</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4>Disfruta!</h4>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Team -->
<section class="page-section section-timeline" id="team">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">El equipo</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="img/team/1.jpg" alt="">
                    <h4>Arnau Barrot</h4>
                    <p class="text-muted">Lead Developer</p>
                    <ul class="list-inline social-buttons">
                        <li class="list-inline-item">
                            <a href="https://gitlab.com/Barrot" target="_blank">
                                <i class="fab fa-gitlab"></i>

                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.linkedin.com/in/arnau-barrot/" target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="img/team/2.jpg" alt="">
                    <h4>Carles Miranda</h4>
                    <p class="text-muted">Lead Developer</p>
                    <ul class="list-inline social-buttons">
                        <li class="list-inline-item">
                            <a href="https://gitlab.com/carles.mr.19" target="_blank">
                                <i class="fab fa-gitlab"></i>

                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.linkedin.com/in/carles-miranda/" target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="img/team/3.jpg" alt="">
                    <h4>Josué Pujol</h4>
                    <p class="text-muted">Lead Developer</p>
                    <ul class="list-inline social-buttons">
                        <li class="list-inline-item">
                            <a href="https://gitlab.com/josu389" target="_blank">
                                <i class="fab fa-gitlab"></i>

                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.linkedin.com/in/josu389/" target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="img/team/4.jpg" alt="">
                    <h4>Andrea Dal Pezzo</h4>
                    <p class="text-muted">Lead Designer</p>
                    <ul class="list-inline social-buttons">
                        <li class="list-inline-item">
                            <a href="https://gitlab.com/andalpezzo" target="_blank">
                                <i class="fab fa-gitlab"></i>

                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://www.linkedin.com/in/andalpezzo/" target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
</section>

<!-- Contact -->
<section class="page-section" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">¿Tienes dudas?</h2>
                <h3 class="section-subheading text-muted">Envianos un mensaje y resolveremos todas tus dudas</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form action="{{route('sendMail')}}" method="POST" id="contactForm" name="sentMessage" novalidate="novalidate" onclick="checkForm()">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control" id="name" type="text" placeholder="Nombre" required="required" data-validation-required-message="Please enter your name.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" id="email" type="email" placeholder="Correo electronico" required="required" data-validation-required-message="Porfavor entra el correo electronico">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea class="form-control" id="message" placeholder="Tu mensaje . . ." required="required" data-validation-required-message="Please enter a message."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <p style="color:white"><input type="checkbox" name="checkbox" value="check"> Acepto los <u>términos y condiciones</u></p>
                            <div id="success"></div>
                            <input type="submit" class="btn btn-primary btn-xl text-uppercase" name="email_submit" value="Envia" onclick="if(!this.form.checkbox.checked){alert('Acepta los términos y condiciones.');return false}"  />

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>