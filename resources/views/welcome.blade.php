<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('theme.head')
    <body id="page-top">

    @include('theme.nav')
    @include('theme.header')
 <!-- Sección principal del Welcome -->
    @include('theme.default')
    @include('theme.footer')

    </body>
</html>
