@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Reservas</h1>
        <div class="row justify-content-center">
            <div class="col">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Actividad</th>
                            <th scope="col">Accion</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($reservas as $actividad)
                            <tr>
                                <th>{{$actividad->title}}</th><th>
                                    <a href="{{route('admin.reservas.show', $actividad->id)}}" class="float-left">
                                        <button type="submit" class="btn-success btn-sm"><i class="far fa-eye"></i></button>
                                    </a>
                                </th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{$reservas->links()}}
    </div>
@endsection