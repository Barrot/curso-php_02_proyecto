@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Usuarios</h1>
        <div class="row justify-content-center">
            <div class="col">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Correo Electronico</th>
                            <th scope="col">Rol</th>
                            <th scope="col">Accion</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <th>{{$user->name}}</th>
                                <th>{{$user->email}}</th>
                                <th>{{implode(', ', $user->roles()->get()->pluck('name')->toArray())}}</th>
                                <th>
                                    <a href="{{route('admin.users.edit', $user->id)}}" class="float-left">
                                        <button type="button" class="btn-primary btn-sm"><i class="far fa-edit"></i>
                                        </button>
                                    </a>

                                    <form action="{{route('admin.users.destroy', $user->id)}}"  method="POST" class="float-left">
                                        @csrf @method('DELETE')
                                        <button type="submit" class="btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
                                    </form>
                                </th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$users->links()}}
            </div>
        </div>
    </div>
@endsection