@extends('layouts.app')

@section('styles')
    <link href="{{asset('css/datechooser/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <form action="{{route('admin.actividades.update', ['event' => $actividad->id])}}" method="POST" enctype="multipart/form-data">
                    @csrf {{method_field('PUT')}}
                    <div class="form-group">
                        <label for="title">Titulo:</label>
                        <input type="text" class="form-control" name="title" value="{{$actividad->title}}">

                        <label for="title">Descripcion:</label>
                        <textarea class="form-control rounded-0" rows="3" name="descripcion">{{$actividad->descripcion}}</textarea>

                        <label for="title">Fecha Inicio:</label>
                        <input size="16" type="text" class="form-control" style="width: 200px; margin-bottom: 10px;background-color: white" id="datetime2" name="inicio" value="{{$actividad->start_date}}" readonly>

                        <label for="title">Fecha Fin:</label>
                        <input size="16" type="text" class="form-control" style="width: 200px; margin-bottom: 10px;background-color: white" id="datetime3" name="fin" value="{{$actividad->end_date}}" readonly>

                        <label for="exampleFormControlFile1">Imagen: </label><br>

                        <figure class="figure">
                            <img src="{{asset('img/actividades/'.$actividad->imagen)}}" class="figure-img img-fluid rounded" style=" width:100%; height: 250px;">
                        </figure>

                        <input type="file" class="form-control-file" name="imagen"><br>

                    </div>
                    <button type="submit" class="btn-primary">Actualizar</button>
                </form>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/locale/es.js"></script>
    <script src="{{asset('js/datechooser/bootstrap-datetimepicker.min.js')}}"></script>

    <script type="text/javascript">

        $("#datetime2").datetimepicker({
            dateFormat: 'dd/mm/yy',
            autoclose: true,
            startDate: new Date()
        });

        $("#datetime3").datetimepicker({
            dateFormat: 'dd/mm/yy',
            autoclose: true,
            startDate: $("#datetime2").datetimepicker("getDate")
        });

    </script>
@endsection