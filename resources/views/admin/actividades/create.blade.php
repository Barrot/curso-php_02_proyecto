@extends('layouts.app')

@section('styles')
    <link href="{{asset('css/datechooser/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">

        @if($errors->any())

            @foreach ($errors->all() as $error)
                <div class="alert alert-danger" role="alert">
                    {{$error}}
                </div>
            @endforeach
        @endif

        <div class="row justify-content-center">
            <div class="col">
                <form method="POST" action="{{route('admin.actividades.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="title">Titulo:</label>
                        <input type="text" class="form-control" name="title" required>

                        <label for="title">Descripcion</label>
                        <textarea class="form-control rounded-0" rows="3" name="descripcion" required ></textarea>

                        <label for="title">Fecha Inicio:</label>
                        <input lang="ar-tn" size="16" type="text" class="form-control" style="width: 200px; margin-bottom: 10px;background-color: white" id="datetime2" name="inicio" required readonly>

                        <label for="title">Fecha Fin:</label>
                        <input size="16" type="text" class="form-control" style="width: 200px; margin-bottom: 10px;background-color: white" id="datetime3" name="fin" required readonly>

                        <label for="exampleFormControlFile1">Imagen: </label>
                        <input type="file" class="form-control-file" name="imagen"><br>

                        <button type="submit" class="btn btn-primary">Añadir</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="{{asset('js/datechooser/bootstrap-datetimepicker.min.js')}}"></script>

    <script type="text/javascript">

        $("#datetime2").datetimepicker({
            dateFormat: 'dd/mm/yy',
            autoclose: true,
            startDate: new Date()
        });

        $("#datetime3").datetimepicker({
            dateFormat: 'dd/mm/yy',
            autoclose: true,
            startDate: $("#datetime2").datetimepicker("getDate")
        });

    </script>
@endsection
