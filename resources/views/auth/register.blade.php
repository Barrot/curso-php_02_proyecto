<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Register</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="googlebot" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script type="text/javascript" src="/js/lib/dummy.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" type="text/css" href="css/estilo.css">

    <!-- TODO: Missing CoffeeScript 2 -->

    <script type="text/javascript">//<![CDATA[
        window.onload=function(){}
        //]]></script>

</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <a href="{{ url('/') }}">
                <img class="logo" src="img/logo.png">
            </a>
            <div class="card card-signin my-5">
                <div class="tituloRojo">
                    <h3>REGISTRO</h3>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-label-group">
                            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Name" @error('name') is-invalid @enderror name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            <label for="inputEmail">Correo Electrónico</label>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-label-group">
                            <input type="text" name="name" id="inputName" class="form-control" placeholder="Email address" @error('name') is-invalid @enderror value="{{ old('email') }}" required autocomplete="email">
                            <label for="inputName">Nombre</label>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-label-group">
                            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" @error('name') is-invalid @enderror value="{{ old('password') }}" required autocomplete="new-password">
                            <label for="inputPassword">Contraseña</label>

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-label-group">
                            <input name="password_confirmation" type="password" id="inputConfirmPassword" class="form-control" placeholder="ConfirmPassword" required autocomplete="new-password">
                            <label for="inputConfirmPassword">Confirmar contraseña</label>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Registrarme</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
