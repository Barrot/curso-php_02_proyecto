@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>{{$evento->title}}
            <a href="{{route('mod.generarPDF', $evento->id)}}" class="float-right">
                <button type="button" class="btn btn-primary">Genearar PDF</button>
            </a>
        </h1>
        <div class="row justify-content-center">
            <div class="col">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($evento->users as $cliente)
                            <tr>
                                <th>{{$cliente->name}}</th>
                                <th>{{$cliente->email}}</th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection