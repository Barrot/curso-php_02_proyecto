@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>{{ $actividad->title }}</h1>

        <div class="row justify-content-center">
            <div class="col">
                <div class="form-group">
                    <h5 class="mt-2">
                        <strong>Titulo: </strong>
                        {{$actividad->title}} <br>
                    </h5>

                    <h5 class="mt-2">
                        <strong>Descripcion: </strong> <br>
                        {{$actividad->descripcion}} <br>
                    </h5>

                    <h5 class="mt-2">
                        <strong>Fecha Inicio: </strong>
                        <br>
                        {{date('d-m-Y H:i', strtotime($actividad->start_date))}}

                    </h5>

                    <h5 class="mt-2">
                        <strong>Fecha Fin: </strong>
                        <br>
                        {{date('d-m-Y H:i', strtotime($actividad->end_date))}} <br>
                    </h5>

                    <figure class="figure">
                        <img src="{{asset('img/actividades/'.$actividad->imagen)}}" class="figure-img img-fluid rounded" style=" width:100%; height: 250px;">
                    </figure>
                </div>

                <a href="{{ URL::previous() }}"><button type="submit" class="btn-primary btn-sm">Volver</button></a>

            </div>
        </div>
    </div>
@endsection


