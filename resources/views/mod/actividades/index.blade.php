@extends('layouts.app')

@section('content')
    <div class="container">

        <div>
            <h1> Actividades

                <a href="{{route('mod.actividades.create')}}" class="float-right">
                    <button type="button" class="btn btn-primary">Añadir Actividad</button>
                </a>
            </h1>
        </div>

        <div class="row justify-content-center">
            <div class="col">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col" >Img</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Fecha Inicio</th>
                        <th scope="col">Fecha Fin</th>
                        <th scope="col">Accion</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($actividades as $actividad)
                        <tr>
                            <th><img src="{{asset('img/actividades/'.$actividad->imagen)}}" class="figure-img img-fluid rounded" style="width: 200px; height: 100px;"></th>
                            <th>{{$actividad->title}}</th>
                            <th>{{date('d-m-Y', strtotime($actividad->start_date))}}
                                <br>
                            {{date('H:i', strtotime($actividad->start_date))}}
                            <th>{{date('d-m-Y', strtotime($actividad->end_date))}}
                                <br>
                                {{date('H:i', strtotime($actividad->end_date))}}</th>
                            <th>
                                <a href="{{route('mod.actividades.edit', $actividad->id)}}" class="float-left">
                                    <button type="button" class="btn-primary btn-sm"><i class="far fa-edit"></i></button>
                                </a>

                                <a href="{{route('mod.actividades.show', $actividad->id)}}" class="float-left mr-2">
                                    <button type="submit" class="btn-success btn-sm"><i class="far fa-eye"></i></button>
                                </a>

                                <form action="{{route('mod.actividades.destroy', $actividad->id)}}"  method="POST" class="float-left">
                                    @csrf @method('DELETE')
                                    <button type="submit" class="btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
                                </form>
                            </th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{$actividades->links()}}
    </div>
@endsection

