<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #212529" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{route('home')}}" >
            <img class="logo" src="{{asset('img/logo.png')}}">
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/home">Inicio <span class="sr-only">(current)</span></a>
                </li>
                @hasrole('admin')
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('admin.users.index')}}">Control Usuarios</a>
                </li>

                <li class="nav-item active">
                    <a class="nav-link" href="{{route('admin.actividades.index')}}">Actividades</a>
                </li>

                <li class="nav-item active">
                    <a class="nav-link" href="{{route('admin.reservas.index')}}">Control Reservas</a>
                </li>
                @endhasrole

                @hasrole('mod')
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('mod.actividades.index')}}">Actividades</a>
                </li>

                <li class="nav-item active">
                    <a class="nav-link" href="{{route('mod.reservas.index')}}">Control Reservas</a>
                </li>
                @endhasrole

                @hasrole('user')
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('user.actividades.index')}}">Listado Actividades</a>
                </li>

                <li class="nav-item active">
                    <a class="nav-link" href="{{route('user.reservas.index')}}">Mis Reservas</a>
                </li>

                <li class="nav-item active">
                    <a class="nav-link" href="{{url('/users')}}">Usuarios</a>
                </li>
                @endhasrole

                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a type="button" class="dropdown-item" data-toggle="modal" data-target="#editarPerfil">
                                Editar Perfil
                            </a>

                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Cerrar sesión') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest

            </ul>
        </div>
    </div>
</nav>

<!-- Modal Editar Usuario -->
<div class="modal fade" id="editarPerfil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Perfil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('editar.perfil', Auth::user()->id)}}" method="POST">
                    @csrf @method('PUT')

                    <label for="title">Nombre:</label>
                    <input type="text" class="form-control" name="name" value="{{Auth::user()->name}}">

                    <label for="title">Correo Electronico:</label>
                    <input type="text" class="form-control" name="email" value="{{Auth::user()->email}}"><br>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary" style="background-color:#FF5757">Guardar</button>
                </form>
            </div>

        </div>
    </div>
</div>