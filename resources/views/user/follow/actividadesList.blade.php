@extends('layouts.app')

@section('content')
    <script src="{{ asset('js/custom.js') }}" defer></script>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        {{ $user->name }}
                        <br/>
                        <small>
                            <strong>Email: </strong>{{ $user->email }}
                        </small>
                    </div>

                    <div class="card-body">
                        <div class="row pl-5">
                            @foreach($actividades as $actividad)
                                    <div class="col-2 profile-box border p-1 rounded text-center bg-light mr-4 mt-3">
                                    <img src="{{{ asset('img/actividades/'.$actividad->imagen) }}}" class="w-100 mb-1" >
                                    <h5 class="m-0"><a href="{{route('user.actividades.show', $actividad->id)}}"><strong>{{$actividad->title}}</strong></a></h5>



                                        @if (auth()->user()->events->contains($actividad))
                                            <form action="{{route('user.reservas.destroy', $actividad->id)}}"  method="POST" class="float-left">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn-danger btn-sm" onclick="return confirm('Estas seguro que quieres cancelar tu reserva?')">Cancelar Reserva</button>
                                            </form>
                                        @else
                                            <a href="actividades/reservar/{{$actividad->id}}" class="float-left">
                                                <button type="button" class="btn btn-info btn-sm text-center">Inscribirme</button>
                                            </a>
                                        @endif

                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

