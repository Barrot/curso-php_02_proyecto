@if($users->count())
    @foreach($users as $user)
        <div class="col-2 profile-box border p-1 rounded text-center bg-light mr-4 mt-3">
            <img src="{{{ asset('img/user.png') }}}" class="w-100 mb-1" >
            <h5 class="m-0"><a href="{{ route('user.view', $user->id) }}"><strong>{{ $user->name }}</strong></a></h5>

            <button class="btn btn-info btn-sm action-follow" data-id="{{ $user->id }}"><strong>
                    @if(auth()->user()->isFollowing($user))
                        <i class="fas fa-user-slash"></i>
                    @else

                        <i class="fas fa-user-plus"></i>
                    @endif
                </strong></button>
        </div>
    @endforeach
@endif
