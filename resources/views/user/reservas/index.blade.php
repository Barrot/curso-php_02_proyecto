@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Fecha Inicio</th>
                            <th scope="col">Fecha Fin</th>
                            <th scope="col">Accion</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($usuario->events as $evento)
                            <tr>
                                <th>{{$evento->title}}</th>
                                <th>{{$evento->start_date}}</th>
                                <th>{{$evento->end_date}}</th>
                                <th>
                                    <form action="{{route('user.reservas.destroy', $evento->id)}}"  method="POST" class="float-left">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn-danger btn-sm" onclick="return confirm('Estas seguro que quieres cancelar tu reserva?')"><i class="far fa-times-circle"></i></button>
                                    </form>
                                </th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>



    </div>
@endsection