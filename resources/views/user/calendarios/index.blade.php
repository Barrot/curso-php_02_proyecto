@extends('layouts.app')

@section('content')
    <script src="{{ asset('js/custom.js') }}" defer></script>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Lista</div>
                    <div class="card-body">
                        <div class="row pl-5">
                            @foreach($calendarios as $calendario)
                                <div class="col-2 profile-box border p-1 rounded text-center bg-light mr-4 mt-3">
                                    <img href="{{{ asset('img/logo.png') }}}" class="w-100 mb-1">
                                    <h5 class="m-0"><a href="SHOW CALENDARIO"><strong>{{ $calendario->nombre }}</strong></a></h5>
                                    @if($calendario->followers->contains($user))

                                        <a href="calendarios/unfollow/{{$calendario->id}}" class="float-left">
                                            <button class="btn btn-info btn-sm action-follow" data-id="{{ $user->id }}><strong> Unfollow </strong></button>
                                        </a>
                                    @else
                                        <a href="calendarios/seguir/{{$calendario->id}}" class="float-left">
                                            <button class="btn btn-info btn-sm action-follow" data-id="{{ $user->id }}><strong> Follow </strong></button>
                                        </a>
                                    @endif
                                </div>

                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

