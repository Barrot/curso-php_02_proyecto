@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row-fluid ">
            <div class="card-group mt-2">
                <div class="card-deck">
                    @foreach($actividades as $actividad)
                        <div class="card" style="width: 18rem;">
                            <img src="{{asset('img/actividades/'.$actividad->imagen)}}" class="figure-img img-fluid rounded" style=" width:100%; height: 250px;">

                            <div class="card-body" style="text-align: center">
                                <h5 class="card-title">{{$actividad->title}}</h5>
                                <h6><b>Fecha inicio:</b>
                                    <br>
                                        {{date('d-m-Y', strtotime($actividad->start_date))}}
                                            <br>
                                        {{date('H:i', strtotime($actividad->start_date))}}
                                </h6>
                                <h6><b>Fecha fin:</b>
                                    <br>
                                        {{date('d-m-Y', strtotime($actividad->end_date))}}
                                            <br>
                                        {{date('H:i', strtotime($actividad->end_date))}}
                                </h6>

                            </div>

                            <div class="card-footer text-muted">
                                <a href="{{route('user.actividades.show', $actividad->id)}}" class="float-left">
                                    <button type="submit" class="btn-success btn-sm"><i class="far fa-eye"></i></button>
                                </a>

                                @if (auth()->user()->events->contains($actividad))
                                    <form action="{{route('user.reservas.destroy', $actividad->id)}}"  method="POST" class="float-left ml-2">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn-danger btn-sm" onclick="return confirm('Estas seguro que quieres cancelar tu reserva?')"><i class="far fa-times-circle"></i></button>
                                    </form>
                                @else
                                    <a href="actividades/reservar/{{$actividad->id}}" class="float-left">
                                        <button type="button" class="btn-primary btn-sm"><i class="fas fa-user-plus"></i></button>
                                    </a>
                                @endif
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>
        </div><br>
        {{$actividades->links()}}
    </div>
@endsection

