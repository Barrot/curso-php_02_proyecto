<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () { return view('welcome'); })->name('welcome');

//Ruta para usuarios y administroador para EventController@index (Calendario)
Route::get('/home', 'EventController@index')->name('home');

//Rutas Administrador
Route::namespace('Admin')->prefix('admin')->middleware(['auth', 'auth.admin'])->name('admin.')->group(function(){
    //Ruta para el Admin\ActividadesController
    Route::resource('/actividades', 'ActividadesController');

    //Ruta para el Admin\UserController
    Route::resource('/users', 'UserController');

    //Ruta para el Admin\ReservasController
    Route::resource('/reservas', 'ReservasController');

    //Ruta para crear el PDF como Administrador
    Route::get('generate-pdf/{id}','ActividadesController@generatePDF')->name('generarPDF');
});

//Rutas Usuarios
Route::namespace('User')->prefix('user')->middleware(['auth', 'auth.user'])->name('user.')->group(function(){
    //Ruta para crear una reserva.
    Route::get('/actividades/reservar/{id}', 'ReservasController@crear');

    //Ruta para el User\ActividadesController
    Route::resource('/actividades', 'ActividadesController');
    //Ruta para el User\ReservasController
    Route::resource('/reservas', 'ReservasController');

});

//Rutas Moderador
Route::namespace('Mod')->prefix('mod')->middleware(['auth', 'auth.mod'])->name('mod.')->group(function(){
    //Ruta para crear una reserva.
    Route::get('/actividades/ver/{id}', 'ActividadesController@ver');

    //Ruta para el Admin\ActividadesController
    Route::resource('/actividades', 'ActividadesController');

    //Ruta para el Admin\ReservasController
    Route::resource('/reservas', 'ReservasController');

    //Ruta para crer el PDF como Moderador
    Route::get('generate-pdf/{id}','ActividadesController@generatePDF')->name('generarPDF');
});

//Rutas para el follow de los usuarios
Route::get('users', 'User\UsuariosController@users')->name('users');
Route::get('user/{id}', 'User\UsuariosController@user')->name('user.view');
Route::post('ajaxRequest', 'User\UsuariosController@ajaxRequest')->name('ajaxRequest');

//Ruta enviar correo.
Route::post('/send', 'MailController@sendMail')->name('sendMail');

Route::put('editarPerfil/{id}', 'HomeController@update')->name('editar.perfil');

