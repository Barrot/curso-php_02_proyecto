$(document).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.action-follow').click(function(){
        var user_id = $(this).data('id');
        var cObj = $(this);
        var c = $(this).parent("div").find(".tl-follower").text();

        $.ajax({
            type:'POST',
            url:'/ajaxRequest',
            data:{user_id:user_id},

            success:function(data){

                if(jQuery.isEmptyObject(data.success.attached)){
                    cObj.find("strong").html("<i class=\"fas fa-user-plus\"></i>");
                }else{
                    cObj.find("strong").html("<i class=\"fas fa-user-slash\"></i>");
                }
            }
        });
    });
});